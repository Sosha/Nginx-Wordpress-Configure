#!/bin/bash 
clear

# Show Welcome Message
stripe() { 
	ss=$(
  	echo "$1" | sed -e 's:\(\\\(e\|033\|x1B\)\)\(\[\([0-9]\{1,3\};\?\)\+m\)::g'); 
  	echo $ss; 
};

centerize() { 
  	strip=$(stripe "$1"); 
  	sps=$(( ($(tput cols) - ${#strip}) / 2 )); 
  	printf "%*s%s%*s\n" $sps " " "$(echo -e $1)" $sps " "; 
};

centerize "Hi \e[0;31m$USER\e[0m,"
centerize "Script for Create Wordpress Nginx"
centerize "Enjoy That! ;)"
sleep 5; clear


# Update and Install Packeges (PHP, MySQL, Git, LetsEncrypt)
sudo apt install -y php7.0-fpm php7.0-mysql php7.0-mcrypt php-mbstring php-gettext php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc mysql git letsencrypt


# Make Directory on /var/www/$MKDC/html/
read -p "Type for Make Directory: (Ex: example.net) " MKDC
sudo mkdir -p /var/www/$MKDC/html/ 
echo -e "$MKDC Created!\n\n"


# Download Wordpress & Extract
cd /tmp

	# Download
	wget https://wordpress.org/latest.tar.gz
	
	# Extract
	tar -xzvf latest.tar.gz
	cp -r /tmp/wordpress/* /var/www/$MKDC/html/
echo -e "Wordpress Downloaded and Extracted on $MKDC.\n\n"

# Chown & Chmod Directory

	# Chown
	read -p "Type Chown User: (Default: www-data) " CUDC
	 if [[ -z $CUDC ]]; then
    	chown -R www-data:www-data /var/www/$MKDC/;
        echo -e "Chown www-data for $MKDC!\n\n"
    else
    	chown -R $CUDC:$CUDC /var/www/$MKDC/;
        echo -e "Chown $CUDC for $MKDC!\n\n"
	fi

	# Chmod
	sudo chmod g+w /var/www/$MKDC/html/wp-content
	sudo chmod -R g+w /var/www/$MKDC/html/wp-content/themes
	sudo chmod -R g+w /var/www/$MKDC/html/wp-content/plugins


# Config PHP
sudo phpenmod mcrypt
sudo phpenmod mbstring
#abol
echo -e "PHP Confing.\n\n"


# Create and Grant Database MySQL
read -p "Type for Database Name: " CNDB
echo "CREATE DATABASE $CNDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;" | mysql -u root -p
read -p "Type for Database User: " CUDB
read -p "Type for Database Password: " CPDB
echo "GRANT ALL ON $CNDB.* TO '$CUDB'@'localhost' IDENTIFIED BY '$CPDB';" | mysql -u root -p
echo "FLUSH PRIVILEGES;" | mysql -u root -p
echo -e "Database is Created and Granted.\n\n"

# SSL & Config 
read -p "Do You Want to SSL $MKDC? " MSDC
    if [[ $MSDC == 'y' || $MSDC == 'Y' || $MSDC == 'yes' || $MSDC == 'Yes' || $MSDC == 'YES' ]]; then 
        sudo systemctl stop nginx
		echo "letsencrypt certonly --authenticator standalone --domains $MKDC"
		sudo systemctl start nginx
        echo "server {
listen 80;
	server_name $MKDC www.$MKDC;
	return 301 https://\$host\$request_uri;
  	}
server {
  	listen 443 ssl;

  	root /var/www/$MKDC/html;

  	index index.html index.php index.htm index.nginx-debian.html;

  	server_name $MKDC www.$MKDC;

	# Certidicate
  	ssl_certificate /etc/letsencrypt/live/$MKDC/fullchain.pem;
 	ssl_certificate_key /etc/letsencrypt/live/$MKDC/privkey.pem;

  	# Secre SSL
  	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  	ssl_prefer_server_ciphers on;
  	ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
  
  	location / {
    	try_files \$uri \$uri/ /index.php?q=\$uri&\$args;
  	}

   	location = /favicon.ico { log_not_found off; access_log off; }
   	location = /robots.txt { log_not_found off; access_log off; allow all; }
   	location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
    	expires max;
        log_not_found off;
    }

   	location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }
}" | sudo tee /etc/nginx/site-avalibale/$MKDC > /dev/null
    else
	    echo "server {
	listen 80;
    listen [::]:80;
 
    root /var/www/$MKDC/html;
    index index.html index.php index.htm index.nginx-debian.html;
 
    server_name $MKDC www.$MKDC;

	
  	location / {
    	try_files \$uri \$uri/ /index.php?q=\$uri&\$args;
  	}

   	location = /favicon.ico { log_not_found off; access_log off; }
   	location = /robots.txt { log_not_found off; access_log off; allow all; }
   	location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
    	expires max;
        log_not_found off;
    }

   	location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    } 
}" sudo tee /etc/nginx/site-avalibale/$MKDC > /dev/null 
	fi 
echo -e "SSL and Config Virtual Host.\n\n"


# Edit BIND9
read -p "Enter Name: (Ex: example) " SBED1
read -p "Enter Class: (Ex: IN, Default: IN) " SBED2
SBED2=${SBED2:-IN}
read -p "Enter Type: (Ex: A, Default: A ) " SBED3
SBED3=${SBED3:-A}
read -p "Enter Server Address: (Default: 138.201.73.190) " SBED4
SBED4=${SBED4:-138.201.73.190}
echo "$SBED1 $SBED2 $SBED3 $SBED4" | sudo tee -a /etc/bind/soshaw.net.db > /dev/null
    
    # Fix BIND9 File
    #fixbind() { gawk -v a=$2 -i inplace '{for (i=1; i<=NF; i++) {printf("%s", $i);for ( i=0; i<a-length($1);i++) {printf(" ");}}print;};' $1;};
    #fixbind
echo -e "Edit BIND9 File!\n"


# Restart Services
	
	# BIND9
	sudo service bind9 restart
    echo -e "Service BIND9 Restarted!\n\n"
	
	# PHP
	sudo systemctl restart php7.0-fpm	

    # Nginx
    sudo nginx -t
    echo -e "\n"
	read -p "Do You Want to Restart Nginx Server? [Y/n] " NGRS
    if [ $NGRS == 'y' ]; then
        sudo systemctl restart nginx; echo -e "Service Nginx Restarted!" 
    else
		echo "Abort."
	fi


# Show Congratulation Message
    echo -e "\n\n Congratulation, Wordpress Configure to Successfully! ;)"
